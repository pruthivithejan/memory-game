import turtle
import pandas
import time


def choose_game():
    while True:
        choice = screen.textinput(
            "CHOOSE A GAME", "Enter the Number:")
        if choice == '1':
            return "us.gif", "50_states.csv", "U.S. States Game", "What's another state's name ?", "50 States", 50
        elif choice == '2':
            return "lk.gif", "25_districts.csv", "Sri Lanka District Game", "What's another district's name ?", "25 Districts", 25
        elif choice == '3':
            return "india.gif", "29_states.csv", "India States Game",  "What's another state's name ?",  "29 States", 29
        else:
            print("Invalid choice. Please enter from the menu.")


def setup_menu():
    turtle.bgcolor("lightblue")
    turtle.title("Game Selection")

    button_us = turtle.Turtle()
    button_us.hideturtle()
    button_us.penup()
    button_us.color("black")
    button_us.goto(-50, 50)
    button_us.write("1. U.S. States", align="center",
                    font=("Arial", 14, "normal"))

    button_lk = turtle.Turtle()
    button_lk.hideturtle()
    button_lk.penup()
    button_lk.color("black")
    button_lk.goto(-50, -50)
    button_lk.write("2. Sri Lanka Districts", align="center",
                    font=("Arial", 14, "normal"))

    button_lk = turtle.Turtle()
    button_lk.hideturtle()
    button_lk.penup()
    button_lk.color("black")
    button_lk.goto(-50, -150)
    button_lk.write("3. India States", align="center",
                    font=("Arial", 14, "normal"))


setup_menu()


def show_answers(data):
    for state in data.state:
        t = turtle.Turtle()
        t.hideturtle()
        t.penup()
        state_data = data[data.state == state]
        t.goto(int(state_data.x), int(state_data.y))
        t.write(state.capitalize())


def display_timer(elapsed_time):
    timer_display.clear()
    timer_display.penup()
    timer_display.goto(200, 200)
    timer_display.pendown()
    timer_display.write(f"Time: {int(elapsed_time)}s",
                        align="right", font=("Arial", 12, "normal"))


setup_menu()

screen = turtle.Screen()

IMAGE, ANSWERS, TITLE, PROMPT, COMMENT, COUNT = choose_game()

screen.clear()
screen.title(TITLE)
screen.addshape(IMAGE)

turtle.shape(IMAGE)

data = pandas.read_csv(ANSWERS)

guessed_states = []
all_states = data.state.to_list()

start_time = time.time()
timer_display = turtle.Turtle()
timer_display.hideturtle()
timer_display.penup()
timer_display.color("black")

while len(guessed_states) < COUNT:
    elapsed_time = time.time() - start_time
    if elapsed_time > 300:
        print("Time's up! Showing all answers.")
        show_answers(data)
        break

    display_timer(elapsed_time)

    answer_state = screen.textinput(title=f"{len(guessed_states)}/{COMMENT} Correct",
                                    prompt=PROMPT).title()

    if answer_state == "Exit":
        missing_states = [
            state for state in all_states if state not in guessed_states]
        new_data = pandas.DataFrame(missing_states)
        new_data.to_csv("states_to_learn.csv")
        turtle.bye()

    if answer_state in all_states:
        guessed_states.append(answer_state)
        t = turtle.Turtle()
        t.hideturtle()
        t.penup()
        state_data = data[data.state == answer_state]
        t.goto(int(state_data.x), int(state_data.y))
        t.write(answer_state.capitalize())

show_answers(data)
turtle.done()
